import React, {useState} from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import * as GLOBAL from '../../constants/index';
import Icon from 'react-native-vector-icons/FontAwesome';

function Search() {
  const [search, setSearch] = useState();

  return (
    <View style={styles.contSearch}>
      <Icon
        style={styles.searchIcon}
        name="search"
        size={20}
        color={GLOBAL.COLOR.TEXTLIGHT}
      />
      <TextInput
        placeholder="Search Movies or Actor"
        placeholderTextColor={GLOBAL.COLOR.TEXTLIGHT}
        onChangeText={(text) => setSearch(search)}
        value={search}
        style={styles.formTextInput}
      />
    </View>
  );
}

export default Search;

const styles = StyleSheet.create({
  contSearch: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255,255 ,255 ,0.2 )',
    borderRadius: 10,
    marginVertical: 10,
  },
  searchIcon: {
    left: 20,
  },
  formTextInput: {
    width: GLOBAL.DIMENSWIDTH * 0.9,
    left: 30,
    color: GLOBAL.COLOR.TEXTLIGHT,
    fontSize: 15,
  },
});
