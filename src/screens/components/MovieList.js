import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Image,
  ActivityIndicator,
} from 'react-native';
import * as GLOBAL from '../../constants/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import {apiDataMovies} from '../../common/api/fakeApi';

function MovieList() {
  const [listGenre, setListGenre] = useState([]);
  const [listMovie, setListMovie] = useState([]);
  const [dataFilter, setDataFilter] = useState([]);
  const [activeIndex, setActiveIndex] = useState('All');

  const pickGenre = (items) => {
    setActiveIndex(items);
    setDataFilter(
      listMovie.filter((item) => {
        return item.genre == items;
      }),
    );
  };
  //filter same genre
  const getUnique = (array) => {
    var uniqueArray = [];
    for (var value of array) {
      if (uniqueArray.indexOf(value) === -1) {
        uniqueArray.push(value);
      }
    }
    return uniqueArray;
  };

  useEffect(() => {
    const fetchData = async () => {
      const result = await apiDataMovies();
      setListMovie(result.data);
      setDataFilter(result.data);
      const temp = [];
      result.data.map((movie) => temp.push(movie.genre));
      temp.filter((a, b) => temp.indexOf(a) === b);
      const unique = getUnique(temp);
      setListGenre(unique);
      console.log(unique);
    };
    fetchData();
  }, []);

  return (
    <View>
      <View style={styles.heading}>
        <Text style={styles.headingPage}>GENRES |</Text>
        <Text style={styles.subHeadingPage}> SWIPE FOR MORE</Text>
      </View>
      <View>
        <ScrollView
          style={{marginVertical: 10}}
          horizontal={true}
          showsHorizontalScrollIndicator={false}>
          {listGenre.map((items, index) => (
            <TouchableOpacity
              onPress={() => {
                pickGenre(items);
              }}
              style={
                activeIndex === items
                  ? GLOBAL.ACTIVEMAINBUTTON
                  : GLOBAL.MAINBUTTON
              }
              key={index}>
              <Text style={GLOBAL.TEXTBUTTON}>{items}</Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
        <View style={styles.heading}>
          <Text style={styles.headingPage}>{activeIndex.toUpperCase()} |</Text>
          <Text style={styles.subHeadingPage}> MOVIES GENRE</Text>
        </View>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{marginBottom: 420}}>
        {dataFilter.map((items, index) => (
          <View style={styles.contCard} key={index}>
            <Image source={{uri: items.image}} style={styles.coverImage} />
            <View style={{padding: 10}}>
              <View style={GLOBAL.FLEXROWCENTER}>
                <Text style={styles.title} numberOfLines={2}>
                  {items.title.toUpperCase()}
                </Text>
              </View>
              <View style={GLOBAL.FLEXROWCENTER}>
                <Text style={styles.title}>{items.rating}/10 |</Text>
                <Text style={styles.title}> {items.genre.toUpperCase()}</Text>
              </View>
              <Text style={styles.synopsis} numberOfLines={2}>
                {items.synopsis}
              </Text>
              <View style={GLOBAL.FLEXROWBETWEEN}>
                <Icon
                  name="comments"
                  type="font-awesome"
                  size={25}
                  color={GLOBAL.COLOR.TEXTDARK}
                  style={{paddingVertical: 10}}
                />
                <Icon
                  name="share-alt"
                  type="font-awesome"
                  size={25}
                  color={GLOBAL.COLOR.TEXTDARK}
                  style={{paddingVertical: 10}}
                />
              </View>
            </View>
          </View>
        ))}
      </ScrollView>
    </View>
  );
}

export default MovieList;

const styles = StyleSheet.create({
  heading: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  headingPage: {
    color: GLOBAL.COLOR.TEXTLIGHT,
    fontSize: 25,
    fontFamily: GLOBAL.FONTBOLD,
  },
  subHeadingPage: {
    color: GLOBAL.COLOR.TEXTLIGHT,
    fontSize: 25,
    fontFamily: GLOBAL.FONTLIGHT,
  },
  contCard: {
    backgroundColor: GLOBAL.COLOR.TEXTLIGHT,
    borderRadius: 10,
    marginVertical: 10,
  },
  coverImage: {
    height: 150,
    width: '100%',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
  synopsis: {
    textAlign: 'justify',
    fontFamily: GLOBAL.FONTREG,
    fontSize: 15,
  },
  title: {
    textAlign: 'justify',
    fontFamily: GLOBAL.FONTBOLD,
    fontSize: 20,
  },
});
