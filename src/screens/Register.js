import React, {useState} from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  StyleSheet,
} from 'react-native';
import * as GLOBAL from '../constants/index';
import {Icon} from 'react-native-elements';

function Register() {
  const [fullname, setFullname] = useState();
  const [username, setUsername] = useState();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  return (
    <View style={GLOBAL.WINDOW}>
      <View style={GLOBAL.CONTAINER}>
        <View style={styles.position}>
          <View style={GLOBAL.CENTER}>
            <Image source={require('../img/logo.png')} style={styles.logo} />
          </View>
          <TextInput
            placeholder="fullname"
            placeholderTextColor={GLOBAL.COLOR.TEXTLIGHT}
            value={fullname}
          />
          <TextInput
            placeholder="username"
            placeholderTextColor={GLOBAL.COLOR.TEXTLIGHT}
            value={username}
          />
          <TextInput
            placeholder="email"
            placeholderTextColor={GLOBAL.COLOR.TEXTLIGHT}
            value={email}
          />
          <TextInput
            placeholder="password"
            placeholderTextColor={GLOBAL.COLOR.TEXTLIGHT}
            value={password}
          />
          <View style={GLOBAL.CENTER}>
            <TouchableOpacity style={GLOBAL.MAINBUTTON}>
              <Text style={GLOBAL.TEXTBUTTON}>REGISTER</Text>
            </TouchableOpacity>

            <TouchableOpacity>
              <Text style={styles.textLink}>
                Already have an account ? LOGIN
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
}

export default Register;

const styles = StyleSheet.create({
  position: {
    position: 'relative',
    top: GLOBAL.DIMENSHEIGHT * 0.12,
  },
  logo: {
    width: 100,
    height: 100,
    marginBottom: 50,
  },
  forgot: {
    marginTop: -10,
    paddingBottom: 20,
    alignItems: 'flex-end',
  },
  textLink: {
    color: GLOBAL.COLOR.SECOND,
    fontFamily: GLOBAL.FONTREG,
    padding: 20,
  },
});
