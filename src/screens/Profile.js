import React, {useState} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Image,
} from 'react-native';
import * as GLOBAL from '../../constants/index';
import Icon from 'react-native-vector-icons/FontAwesome';

function MovieList() {
  const [listGenre, setListGenre] = useState([
    'Action',
    'Drama',
    'Romance',
    'Adventure',
    'Adult',
  ]);
  const [activeIndex, setActiveIndex] = useState('All');

  return (
    <View>
      <View style={styles.heading}>
        <Text style={styles.headingPage}>GENRES |</Text>
        <Text style={styles.subHeadingPage}> SWIPE FOR MORE</Text>
      </View>
      <View>
        <ScrollView
          style={styles.contGenre}
          horizontal={true}
          showsHorizontalScrollIndicator={false}>
          {listGenre.map((items, index) => (
            <TouchableOpacity
              onPress={() => {
                setActiveIndex(items);
              }}
              style={
                activeIndex === items
                  ? GLOBAL.ACTIVEMAINBUTTON
                  : GLOBAL.MAINBUTTON
              }
              key={index}>
              <Text style={GLOBAL.TEXTBUTTON}>{items}</Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
        <View style={styles.heading}>
          <Text style={styles.headingPage}>{activeIndex.toUpperCase()} |</Text>
          <Text style={styles.subHeadingPage}> MOVIES GENRE</Text>
        </View>
      </View>
      <View>
        <View
          style={{
            backgroundColor: '#c4c4c4',
            borderRadius: 10,
            marginVertical: 10,
          }}>
          <Image
            source={require('../../img/poster.png')}
            style={{
              height: 150,
              width: '100%',
              borderTopRightRadius: 10,
              borderTopLeftRadius: 10,
            }}
          />
          <View style={{padding: 10, textAlign: 'justify'}}>
            <Text style={{textAlign: 'justify'}}>
              {' '}
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero
              minus placeat minima totam dignissimos consequatur magni sequi! Ex
              unde iure ullam, ipsum, pariatur aut veritatis.
            </Text>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Icon
                name="comments"
                type="font-awesome"
                size={25}
                color={GLOBAL.COLOR.TEXTDARK}
                style={{paddingVertical: 10}}
              />
              <Icon
                name="share-alt"
                type="font-awesome"
                size={25}
                color={GLOBAL.COLOR.TEXTDARK}
                style={{paddingVertical: 10}}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

export default MovieList;

const styles = StyleSheet.create({
  heading: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  headingPage: {
    color: GLOBAL.COLOR.TEXTLIGHT,
    fontSize: 25,
    fontFamily: GLOBAL.FONTBOLD,
  },
  subHeadingPage: {
    color: GLOBAL.COLOR.TEXTLIGHT,
    fontSize: 25,
    fontFamily: GLOBAL.FONTLIGHT,
  },
  contGenre: {
    marginVertical: 10,
  },
});
