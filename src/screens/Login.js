import React, {useState} from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  StyleSheet,
} from 'react-native';
import * as GLOBAL from '../constants/index';
import {Icon} from 'react-native-elements';

function Login() {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();

  return (
    <View style={GLOBAL.WINDOW}>
      <View style={GLOBAL.CONTAINER}>
        <View style={styles.position}>
          <View style={GLOBAL.CENTER}>
            <Image source={require('../img/logo.png')} style={styles.logo} />
          </View>
          <TextInput
            placeholder="username"
            placeholderTextColor={GLOBAL.COLOR.TEXTLIGHT}
            value={username}
          />
          <TextInput
            placeholder="password"
            placeholderTextColor={GLOBAL.COLOR.TEXTLIGHT}
            value={password}
          />
          <View style={styles.forgot}>
            <Text style={{color: GLOBAL.COLOR.WARN}}>forgot password ?</Text>
          </View>
          <View style={GLOBAL.CENTER}>
            <TouchableOpacity style={GLOBAL.MAINBUTTON}>
              <Text style={GLOBAL.TEXTBUTTON}>LOGIN</Text>
            </TouchableOpacity>

            <TouchableOpacity>
              <Text style={styles.textLink}>
                Dont have an account ? REGISTER
              </Text>
            </TouchableOpacity>
            <Text style={{color: GLOBAL.COLOR.TEXTLIGHT}}>or LOGIN with</Text>
            <View style={styles.socialIcon}>
              <Icon
                name="facebook"
                type="font-awesome"
                size={30}
                color={GLOBAL.COLOR.TEXTLIGHT}
                style={styles.icon}
              />
              <Icon
                name="google"
                type="font-awesome"
                size={30}
                color={GLOBAL.COLOR.TEXTLIGHT}
                style={styles.icon}
              />
              <Icon
                name="linkedin"
                type="font-awesome"
                size={30}
                color={GLOBAL.COLOR.TEXTLIGHT}
                style={styles.icon}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

export default Login;

const styles = StyleSheet.create({
  position: {
    position: 'relative',
    top: GLOBAL.DIMENSHEIGHT * 0.12,
  },
  logo: {
    width: 100,
    height: 100,
    marginBottom: 50,
  },
  forgot: {
    marginTop: -10,
    paddingBottom: 20,
    alignItems: 'flex-end',
  },
  textLink: {
    color: GLOBAL.COLOR.SECOND,
    fontFamily: GLOBAL.FONTREG,
    padding: 20,
  },
  socialIcon: {
    display: 'flex',
    flexDirection: 'row',
    padding: 20,
  },
  icon: {
    paddingHorizontal: 30,
  },
});
