import React, {useState} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import * as GLOBAL from '../constants/index';
import Search from './components/Search';
import MovieList from './components/MovieList';

function HomePage() {
  return (
    <View style={GLOBAL.WINDOW}>
      <View style={GLOBAL.CONTAINER}>
        <Search />
        <MovieList />
      </View>
    </View>
  );
}

export default HomePage;

const styles = StyleSheet.create({});
