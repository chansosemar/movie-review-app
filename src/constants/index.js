import {Dimensions} from 'react-native';

export const DIMENSHEIGHT = Dimensions.get('window').height;
export const DIMENSWIDTH = Dimensions.get('window').width;

// STYLE DEFAULT
export const COLOR = {
  BASE: '#222',
  MAIN: '#335C67',
  SECOND: '#E09F3E',
  WARN: '#9E2A2B',
  TEXTLIGHT: '#C4C4C4',
  TEXTDARK: '#000',
};
export const MAINBUTTON = {
  backgroundColor: COLOR.MAIN,
  width: DIMENSWIDTH * 0.3,
  height: 50,
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 20,
  marginHorizontal: 10,
};
export const ACTIVEMAINBUTTON = {
  backgroundColor: COLOR.SECOND,
  width: DIMENSWIDTH * 0.3,
  height: 50,
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 20,
  marginHorizontal: 10,
};
export const TEXTBUTTON = {color: COLOR.TEXTLIGHT, fontFamily: 'Roboto-Bold'};
export const FONTBOLD = 'Roboto-Black';
export const FONTREG = 'Roboto-Regular';
export const FONTLIGHT = 'Roboto-Thin';
export const CENTER = {alignItems: 'center'};
export const FLEXROWBETWEEN = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
};
export const FLEXROWCENTER = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
};

// CONTAINER DEFAULT
export const WINDOW = {
  backgroundColor: COLOR.BASE,
  height: DIMENSHEIGHT,
  alignItems: 'center',
};
export const CONTAINER = {width: DIMENSWIDTH * 0.9, paddingVertical: 20};
